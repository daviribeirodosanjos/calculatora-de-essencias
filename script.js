function soma() {
    const resultado = document.getElementById('resultado')

    const e450 = document.getElementById('essencia450').value
    const e1350 = document.getElementById('essencia1350').value
    const e3150 = document.getElementById('essencia3150').value
    const e4800 = document.getElementById('essencia4800').value
    const e6300 = document.getElementById('essencia6300').value

    let arr = [e450, e1350, e3150, e4800, e6300]

    for(let i = 0; i < arr.length; i++) {
        if(isNaN(arr[i])) {
            arr[i] = 0
        }
    }

    let soma = 90 * e450 + 270 * e1350 + 630 * e3150 + 960 * e4800 + e6300 * 1260
    resultado.innerHTML = `Você tem ${soma} essências`
}